# Pre-Assessment Spring Starter

## Usage
This starter utilizes Gradle as its build tool. After cloning and changing into its directory, open the the project in your IDE of choice. IntelliJ Idea Community Edition is recommended, however you may wish to use Visual Code Studio if it is set up for Java development with the correct extensions.

Please refer to the [Gradle Wrapper docs](https://docs.gradle.org/current/userguide/gradle_wrapper.html) for further info on building, running, and testing the application.

Simple use cases will involve these commands:
- `./gradlew bootRun` to start the Tomcat server with your application (Default port is `localhost:8080`)
- `./gradlew test -i` in order to run your tests and see the output in the terminal
- `./gradlew build` to create a build of the application for use in web server or deployment

You can leverage the UI in your IDE to run these tasks as well.

# Docker Image

## Installing Docker
- Make sure you installed Docker into your machine
- You can check [Install Docker](https://docs.docker.com/get-docker/) to install Docker

## Pulling docker image from dockerhub
- Run this command in your command line to pull the image into you local machine
```docker
docker pull hdo9/apidemo:latest
```

## Checking your pulled image
- Run this command in your command line to show all images you have
```docker
docker images
```
- Now you should see the image with name `hdo9/apidemo`

## Running docker image

### Running
- Run this command in your command line start your container with name `apidemotrial`
```docker
docker run -it -d -p 8080:8080 --name apidemotrial hdo9/apidemo
```
- The application is running now, to test it, you can add this url in your browser: http://localhost:8080/
- You should see `WELCOME TO HUNG DO'S HOME PAGE`

### Checking running container
- Run this command in your command line will show all running containers
```docker
docker ps
```
- Now you should see your running container at port `8080` and image name is `hdo9/apidemo`
- Run this command in your command will show all containers either running or not
```docker
docker ps -a
```

### Stopping
- Run this command in your command line to stop `apidemotrial` running container
```docker
docker stop apidemotrial
```
- Run this command in your command line to remove `apidemotrial` container
```docker
docker rm apidemotrial
```

### All In One
- Run this command to start `apidemotrial` container and will remove this container when it is stopped
```docker
docker run -it --rm -d -p 8080:8080 --name apidemotrial hdo9/apidemo
```

## How I build my docker image

### Build application
```gradle
./gradlew clean build test
```
- This command will create a jar file inside `/build/libs` folder that is used in dockerfile

### Create Dockerfile
```docker
FROM openjdk:8-jdk-alpine

WORKDIR /

COPY build/libs/pa-spring-starter-0.0.1-SNAPSHOT.jar apidemo.jar

ARG INPUT_PORT

ENV PORT $INPUT_PORT

EXPOSE $PORT

CMD java -jar apidemo.jar
```

### Build docker image
```docker
docker build -t hdo9/apidemo --build-arg INPUT_PORT=8080 .
```
- You can replace the 'hdo9/apidemo' by any name you want for your image.
- You can give input for the port inside container that is used to map to your local port.

