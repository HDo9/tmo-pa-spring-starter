FROM openjdk:8-jdk-alpine

WORKDIR /

COPY build/libs/pa-spring-starter-0.0.1-SNAPSHOT.jar apidemo.jar

ARG INPUT_PORT
ENV PORT $INPUT_PORT
EXPOSE $PORT

CMD java -jar apidemo.jar